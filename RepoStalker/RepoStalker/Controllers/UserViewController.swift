//
//  UserViewController.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation
import UIKit

class UserViewController: UIViewController {
    private let githubUser: GitHubUser

    private var headerView: UIView!
    private var bgView: DiagonalGradientView!
    private var avatarView: UIImageView!
    private var fullNameLabel: UILabel!
    private var usernameLabel: UILabel!
    private var joinDateLabel: UILabel!

    private let RepoReuseIdentifier = "RepoReuseIdentifier"
    private var repoTableView: UITableView!
    private var repoTableViewHeightConstraint: NSLayoutConstraint!

    private var githubRepos: [GithubRepo]?

    private var scrollView: UIScrollView { return view as! UIScrollView }

    private var imageDownloadTask: URLSessionDataTask?

    init(githubUser: GitHubUser) {
        self.githubUser = githubUser

        super.init(nibName: nil, bundle: nil)
    }

    deinit {
        imageDownloadTask?.cancel()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not implemented; use designated initializer instead")
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        fatalError("init(nibName:bundle:) not implemented; use designated initializer instead")
    }

    override func loadView() {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.contentInsetAdjustmentBehavior = .never
        view = scrollView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        navigationItem.title = githubUser.username
        navigationItem.titleView = UIView(frame: .zero)

        headerView = UIView(frame: .zero)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = .gray
        headerView.layer.masksToBounds = false
        headerView.layer.shadowColor = UIColor.black.cgColor
        headerView.layer.shadowOpacity = 0.5
        headerView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        headerView.layer.shadowRadius = 4.0

        bgView = DiagonalGradientView(frame: .zero)
        bgView.translatesAutoresizingMaskIntoConstraints = false
        bgView.startColor = .limeGreen
        bgView.endColor = .royalBlue

        avatarView = UIImageView(frame: .zero)
        avatarView.translatesAutoresizingMaskIntoConstraints = false
        avatarView.contentMode = .scaleAspectFit
        avatarView.backgroundColor = .deepGray
        avatarView.layer.masksToBounds = false
        avatarView.layer.shadowColor = UIColor.black.cgColor
        avatarView.layer.shadowOpacity = 0.5
        avatarView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        avatarView.layer.shadowRadius = 4.0

        if let avatarUrl = githubUser.avatarUrl {
            imageDownloadTask?.cancel()
            imageDownloadTask = URLSession.shared.dataTask(with: avatarUrl) { [weak self] (data, response, error) in
                guard error == nil, let data = data else { return /* TODO: Display placeholder avatar */ }

                DispatchQueue.main.async {
                    self?.avatarView.image = UIImage(data: data)
                }
            }
            imageDownloadTask?.resume()
        }

        fullNameLabel = UILabel(frame: .zero)
        fullNameLabel.translatesAutoresizingMaskIntoConstraints = false
        fullNameLabel.text = githubUser.fullName
        fullNameLabel.textAlignment = .center
        fullNameLabel.textColor = .white
        fullNameLabel.font = UIFont.systemFont(ofSize: 24.0, weight: .bold)

        usernameLabel = UILabel(frame: .zero)
        usernameLabel.translatesAutoresizingMaskIntoConstraints = false
        usernameLabel.text = "@" + githubUser.username
        usernameLabel.textAlignment = .center
        usernameLabel.textColor = .white
        usernameLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)

        joinDateLabel = UILabel(frame: .zero)
        joinDateLabel.translatesAutoresizingMaskIntoConstraints = false
        joinDateLabel.textAlignment = .center
        joinDateLabel.textColor = .white60
        let joinDateText = NSAttributedString(string: ("JOINED " + githubUser.joinDate).uppercased())
        joinDateLabel.attributedText = joinDateText.attributedString(
            font: UIFont.systemFont(ofSize: 13.0, weight: .light),
            spacing: 3
        )

        headerView.addSubview(bgView)
        headerView.addSubview(avatarView)
        headerView.addSubview(fullNameLabel)
        headerView.addSubview(usernameLabel)
        headerView.addSubview(joinDateLabel)

        repoTableView = UITableView(frame: .zero, style: .plain)
        repoTableView.translatesAutoresizingMaskIntoConstraints = false
        repoTableView.backgroundColor = .clear
        repoTableView.isScrollEnabled = false
        repoTableView.dataSource = self
        repoTableView.delegate = self
        repoTableView.register(RepoTableViewCell.self, forCellReuseIdentifier: RepoReuseIdentifier)

        view.addSubview(headerView)
        view.addSubview(repoTableView)

        setupConstraints()

        fetchRepos()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        headerView.layer.shadowPath = UIBezierPath(rect: headerView.bounds).cgPath
        avatarView.layer.shadowPath = UIBezierPath(rect: avatarView.bounds).cgPath
        updateTableViewHeight()
    }

    func refreshRepos() {
        repoTableView.reloadData()
        updateTableViewHeight()
    }

    func updateTableViewHeight() {
        repoTableViewHeightConstraint.constant = repoTableView.contentSize.height
    }

    func setupConstraints() {
        repoTableViewHeightConstraint = repoTableView.heightAnchor.constraint(equalToConstant: 0)

        let constraints: [NSLayoutConstraint] = [
            headerView.widthAnchor.constraint(equalTo: view.widthAnchor),
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            bgView.topAnchor.constraint(equalTo: headerView.topAnchor),
            bgView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            bgView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            bgView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),

            avatarView.widthAnchor.constraint(equalToConstant: 128.0),
            avatarView.heightAnchor.constraint(equalToConstant: 128.0),
            avatarView.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 96.0),
            avatarView.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),

            fullNameLabel.topAnchor.constraint(equalTo: avatarView.bottomAnchor, constant: 16.0),
            fullNameLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0),
            fullNameLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16.0),

            usernameLabel.topAnchor.constraint(equalTo: fullNameLabel.bottomAnchor, constant: 8.0),
            usernameLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0),
            usernameLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16.0),

            joinDateLabel.topAnchor.constraint(equalTo: usernameLabel.bottomAnchor, constant: 32.0),
            joinDateLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0),
            joinDateLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16.0),
            joinDateLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -32.0),

            repoTableView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -32.0),
            repoTableViewHeightConstraint,
            repoTableView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 32.0),
            repoTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0),
            repoTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0),
            repoTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32.0),
        ]

        NSLayoutConstraint.activate(constraints)
    }

    func fetchRepos() {
        guard let reposURL = URL.githubRepos(for: githubUser.username) else { return /* TODO: Handle invalid repos URL */ }

        let reposTask = URLSession.shared.dataTask(with: reposURL) { [weak self] (data, response, error) in
            guard error == nil, let data = data else { return /*
                 TODO: Handle error loading repos */ }

            guard let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments),
                let array = json as? Array<Dictionary<String, Any>> else { return /* TODO: Handle error JSON parsing */ }

            var githubRepos: [GithubRepo] = []

            for dict in array {
                let repo = GithubRepo(dictionary: dict)
                if !repo.name.isEmpty {
                    githubRepos.append(repo)
                }
            }

            DispatchQueue.main.async {
                self?.githubRepos = githubRepos
                self?.refreshRepos()
            }
        }
        reposTask.resume()
    }
}

extension UserViewController: UIScrollViewDelegate {

}

extension UserViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepoReuseIdentifier, for: indexPath)
        guard let repoCell = cell as? RepoTableViewCell else { return cell }

        if let repo = githubRepos?[indexPath.row] {
            repoCell.name = repo.name
            repoCell.desc = repo.desc
            repoCell.stars = repo.starsCount
            repoCell.forks = repo.forksCount
        }

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return githubRepos?.count ?? 0
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.0
    }
}

extension UserViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: .zero)
        label.textColor = .black
        let attributedText = NSAttributedString(string: String(githubRepos?.count ?? 0) + " REPOSITORIES".uppercased())
        label.attributedText = attributedText.attributedString(
            font: UIFont.systemFont(ofSize: 13.0, weight: .light),
            spacing: 3
        )
        return label
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 16.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Remove selection highlight
        tableView.deselectRow(at: indexPath, animated: true)

        if let repo = githubRepos?[indexPath.row] {
            let repoVC = RepoViewController(githubRepo: repo)
            navigationController?.pushViewController(repoVC, animated: true)
        }
    }
}
