//
//  RepoViewController.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation
import UIKit

class RepoViewController: UIViewController {
    private let githubRepo: GithubRepo

    private var headerView: UIView!
    private var bgView: DiagonalGradientView!
    private var nameLabel: UILabel!
    private var descriptionLabel: UILabel!
    private var statsView: UIView!
    private var starsLabel: UILabel!
    private var forksLabel: UILabel!
    private var cloneUrlLabel: UILabel!
    private var sshUrlLabel: UILabel!

    private let CommitReuseIdentifier = "CommitReuseIdentifier"
    private var commitsTableView: UITableView!
    private var commitsTableViewHeightConstraint: NSLayoutConstraint!
    private var commits: [GithubCommit]?

    private var scrollView: UIScrollView { return view as! UIScrollView }

    init(githubRepo: GithubRepo) {
        self.githubRepo = githubRepo

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not implemented; use designated initializer instead")
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        fatalError("init(nibName:bundle:) not implemented; use designated initializer instead")
    }

    override func loadView() {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.contentInsetAdjustmentBehavior = .never
        view = scrollView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white

        headerView = UIView(frame: .zero)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = .gray
        headerView.layer.masksToBounds = false
        headerView.layer.shadowColor = UIColor.black.cgColor
        headerView.layer.shadowOpacity = 0.5
        headerView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        headerView.layer.shadowRadius = 4.0

        bgView = DiagonalGradientView(frame: .zero)
        bgView.translatesAutoresizingMaskIntoConstraints = false
        bgView.startColor = .limeGreen
        bgView.endColor = .royalBlue

        nameLabel = UILabel(frame: .zero)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.text = githubRepo.name
        nameLabel.textAlignment = .center
        nameLabel.textColor = .white
        nameLabel.font = UIFont.systemFont(ofSize: 24.0, weight: .bold)

        descriptionLabel = UILabel(frame: .zero)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.text = githubRepo.desc
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center
        descriptionLabel.textColor = .white
        descriptionLabel.font = UIFont.systemFont(ofSize: 16.0)

        statsView = UIView(frame: .zero)
        statsView.translatesAutoresizingMaskIntoConstraints = false

        starsLabel = UILabel(frame: .zero)
        starsLabel.translatesAutoresizingMaskIntoConstraints = false
        starsLabel.text = "★ " + String(githubRepo.starsCount)
        starsLabel.textColor = .white
        starsLabel.font = UIFont.systemFont(ofSize: 16.0)

        forksLabel = UILabel(frame: .zero)
        forksLabel.translatesAutoresizingMaskIntoConstraints = false
        forksLabel.text = "⑂ " + String(githubRepo.forksCount)
        forksLabel.textColor = .white
        forksLabel.font = UIFont.systemFont(ofSize: 16.0)

        statsView.addSubview(starsLabel)
        statsView.addSubview(forksLabel)

        cloneUrlLabel = UILabel(frame: .zero)
        cloneUrlLabel.translatesAutoresizingMaskIntoConstraints = false
        cloneUrlLabel.text = "HTTPS   •   " + (githubRepo.cloneUrl ?? URL(string: "")!).absoluteString
        cloneUrlLabel.textColor = .white60
        cloneUrlLabel.textAlignment = .center
        cloneUrlLabel.font = UIFont.systemFont(ofSize: 13.0)

        sshUrlLabel = UILabel(frame: .zero)
        sshUrlLabel.translatesAutoresizingMaskIntoConstraints = false
        sshUrlLabel.text = "SSH   •   " + (githubRepo.sshUrl ?? URL(string: "")!).absoluteString
        sshUrlLabel.textColor = .white60
        sshUrlLabel.textAlignment = .center
        sshUrlLabel.font = UIFont.systemFont(ofSize: 13.0)

        headerView.addSubview(bgView)
        headerView.addSubview(nameLabel)
        headerView.addSubview(descriptionLabel)
        headerView.addSubview(statsView)
        headerView.addSubview(cloneUrlLabel)
        headerView.addSubview(sshUrlLabel)

        commitsTableView = UITableView(frame: .zero, style: .plain)
        commitsTableView.translatesAutoresizingMaskIntoConstraints = false
        commitsTableView.backgroundColor = .clear
        commitsTableView.isScrollEnabled = false
        commitsTableView.dataSource = self
        commitsTableView.delegate = self
        commitsTableView.register(CommitTableViewCell.self, forCellReuseIdentifier: CommitReuseIdentifier)

        view.addSubview(headerView)
        view.addSubview(commitsTableView)

        setupConstraints()

        fetchCommits()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        headerView.layer.shadowPath = UIBezierPath(rect: headerView.bounds).cgPath
        updateTableViewHeight()
    }

    func refreshCommits() {
        commitsTableView.reloadData()
        updateTableViewHeight()
    }

    func updateTableViewHeight() {
        commitsTableViewHeightConstraint.constant = commitsTableView.contentSize.height
    }

    func setupConstraints() {
        commitsTableViewHeightConstraint = commitsTableView.heightAnchor.constraint(equalToConstant: 0)

        let constraints: [NSLayoutConstraint] = [
            headerView.widthAnchor.constraint(equalTo: view.widthAnchor),
            headerView.topAnchor.constraint(equalTo: view.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            bgView.topAnchor.constraint(equalTo: headerView.topAnchor),
            bgView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            bgView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            bgView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),

            nameLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 96.0),
            nameLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0),
            nameLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16.0),

            descriptionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8.0),
            descriptionLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0),
            descriptionLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16.0),

            statsView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 16.0),
            statsView.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),

            starsLabel.topAnchor.constraint(equalTo: statsView.topAnchor),
            starsLabel.leadingAnchor.constraint(equalTo: statsView.leadingAnchor),
            starsLabel.bottomAnchor.constraint(equalTo: statsView.bottomAnchor),

            forksLabel.topAnchor.constraint(equalTo: statsView.topAnchor),
            forksLabel.leadingAnchor.constraint(equalTo: starsLabel.trailingAnchor, constant: 8.0),
            forksLabel.trailingAnchor.constraint(equalTo: statsView.trailingAnchor),
            forksLabel.bottomAnchor.constraint(equalTo: statsView.bottomAnchor),

            cloneUrlLabel.topAnchor.constraint(equalTo: statsView.bottomAnchor, constant: 32.0),
            cloneUrlLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0),
            cloneUrlLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16.0),

            sshUrlLabel.topAnchor.constraint(equalTo: cloneUrlLabel.bottomAnchor, constant: 8.0),
            sshUrlLabel.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 16.0),
            sshUrlLabel.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -16.0),
            sshUrlLabel.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -32.0),

            commitsTableView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -32.0),
            commitsTableViewHeightConstraint,
            commitsTableView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 32.0),
            commitsTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0),
            commitsTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0),
            commitsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32.0),
        ]

        NSLayoutConstraint.activate(constraints)
    }

    func fetchCommits() {
        guard let commitsURL = URL.githubCommits(for: githubRepo.username, repo: githubRepo.name) else { return /* TODO: Handle invalid commits URL */ }

        let commitsTask = URLSession.shared.dataTask(with: commitsURL) { [weak self] (data, response, error) in
            guard error == nil, let data = data else { return /*
                 TODO: Handle error loading commits */ }

            guard let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments),
                let array = json as? Array<Dictionary<String, Any>> else { return /* TODO: Handle error JSON parsing */ }

            var commits: [GithubCommit] = []

            for dict in array {
                let commit = GithubCommit(dictionary: dict)
                if !commit.sha.isEmpty {
                    commits.append(commit)
                }
            }

            DispatchQueue.main.async {
                self?.commits = commits
                self?.refreshCommits()
            }
        }
        commitsTask.resume()
    }
}

extension RepoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommitReuseIdentifier, for: indexPath)
        guard let commitCell = cell as? CommitTableViewCell else { return cell }

        if let githubCommit = commits?[indexPath.row] {
            commitCell.sha = githubCommit.sha
            commitCell.authorName = githubCommit.authorName
            commitCell.message = githubCommit.message
        }
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commits?.count ?? 0
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
}

extension RepoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: .zero)
        label.textColor = .black
        let attributedText = NSAttributedString(string: String(commits?.count ?? 0) + " Commits".uppercased())
        label.attributedText = attributedText.attributedString(
            font: UIFont.systemFont(ofSize: 13.0, weight: .light),
            spacing: 3
        )
        return label
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 16.0
    }
}
