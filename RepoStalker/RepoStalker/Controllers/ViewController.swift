//
//  ViewController.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var appTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var usernameFieldLabel: UILabel!

    private var session: URLSession!
    private var currentTask: URLSessionDataTask?
    private var errorView: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()

        session = URLSession.shared

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear

        if let usernameText = usernameFieldLabel.attributedText {
            usernameFieldLabel.attributedText = usernameText.attributedString(
                font: UIFont.systemFont(ofSize: 13.0, weight: .light),
                spacing: 3
            )
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func searchButtonTapped(_ sender: UITextField) {
        sender.resignFirstResponder()

        guard
            let username = sender.text,
            !username.isEmpty,
            let userURL = URL.githubUser(with: username)
            else { return /* TODO: Correctly handle invalid URL errors */ }

        let dataTask = session.dataTask(with: userURL) { [weak self] (data, response, error) in
            guard error == nil, let data = data else { return /* TODO: Handle failed network requests */ }

            guard let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments),
                let dictionary = json as? Dictionary<String, Any> else { return /* TODO: Handle failed JSON parsing */ }

            let githubUser = GitHubUser(with: dictionary)

            DispatchQueue.main.async {
                if githubUser.username.isEmpty {
                    self?.displayUserNotFoundErrorView(username: username)
                } else {
                    let userVC = UserViewController(githubUser: githubUser)
                    self?.navigationController?.pushViewController(userVC, animated: true)
                }
            }
        }
        dataTask.resume()
    }

    func displayUserNotFoundErrorView(username: String) {
        // TODO: Use localized strings
        let errorView = ErrorDialogView(
            header: "User not found",
            message: "We couldn’t find @" + username + " on Github.\n\nPlease double check the username and try again."
        )
        errorView.translatesAutoresizingMaskIntoConstraints = false
        errorView.layer.zPosition = view.layer.zPosition + 1
        errorView.okButton.addTarget(self, action: #selector(dismissError), for: .touchUpInside)
        errorView.alpha = 0.0
        view.addSubview(errorView)

        self.errorView = errorView

        let constraints = [
            errorView.widthAnchor.constraint(equalTo: view.widthAnchor),
            errorView.heightAnchor.constraint(equalTo: view.heightAnchor),
            errorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            errorView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        ]
        NSLayoutConstraint.activate(constraints)

        UIView.animate(withDuration: 0.2) {
            errorView.alpha = 1.0
        }
    }

    @objc func dismissError() {
        UIView.animate(withDuration: 0.2, animations: {
            self.errorView?.alpha = 0.0
        }) { _ in
            self.errorView?.removeFromSuperview()
            self.errorView = nil
        }
    }
}
