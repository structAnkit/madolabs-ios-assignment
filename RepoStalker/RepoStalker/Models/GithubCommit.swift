//
//  GithubCommit.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

class GithubCommit {
    public let sha: String
    public let authorName: String
    public let message: String

    init(dictionary: Dictionary<String, Any>) {
        if let sha = dictionary["sha"] as? String {
            self.sha = String(sha.prefix(7))
        } else {
            self.sha = ""
        }

        let commit = dictionary["commit"] as? Dictionary<String, Any> ?? [:]
        message = commit["message"] as? String ?? ""

        let author = commit["author"] as? Dictionary<String, Any> ?? [:]
        authorName = author["name"] as? String ?? ""
    }
}

