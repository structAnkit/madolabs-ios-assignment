//
//  GithubRepo.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation

class GithubRepo {
    public let username: String
    public let name: String
    public let desc: String
    public let starsCount: Int
    public let forksCount: Int
    public let cloneUrl: URL?
    public let sshUrl: URL?

    init(dictionary: Dictionary<String, Any>) {
        let owner = dictionary["owner"] as? Dictionary<String, Any> ?? [:]
        username = owner["login"] as? String ?? ""

        name = dictionary["name"] as? String ?? ""
        desc = dictionary["description"] as? String ?? ""
        starsCount = dictionary["stargazers_count"] as? Int ?? 0
        forksCount = dictionary["forks"] as? Int ?? 0

        let cloneUrlString = dictionary["clone_url"] as? String ?? ""
        cloneUrl = URL(string: cloneUrlString)

        let sshUrlString = dictionary["ssh_url"] as? String ?? ""
        sshUrl = URL(string: sshUrlString)
    }
}
