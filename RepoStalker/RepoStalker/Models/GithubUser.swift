//
//  GithubUser.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation

class GitHubUser {
    public let username: String
    public let fullName: String
    public let avatarUrl: URL?

    private let displayFormatter: DateFormatter
    private let createdAt: Date
    public var joinDate: String {
        return displayFormatter.string(from: createdAt)
    }

    init(with dictionary: Dictionary<String, Any>) {
        fullName = dictionary["name"] as? String ?? ""
        username = dictionary["login"] as? String ?? ""

        let avatarUrlString = dictionary["avatar_url"] as? String ?? ""
        avatarUrl = URL(string: avatarUrlString)

        let dateString = dictionary["created_at"] as? String ?? ""
        if dateString.isEmpty {
            createdAt = Date()
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            createdAt = formatter.date(from: dateString) ?? Date()
        }

        displayFormatter = DateFormatter()
        displayFormatter.dateFormat = "MMM yyyy"
    }
}
