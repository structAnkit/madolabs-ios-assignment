//
//  RepoTableViewCell.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation
import UIKit

class RepoTableViewCell: UITableViewCell {
    public var name: String? {
        didSet {
            nameLabel.text = name
        }
    }

    public var desc: String? {
        didSet {
            descriptionLabel.text = desc
        }
    }

    public var stars: Int? {
        didSet {
            starsLabel.text = "★ " + String(stars ?? 0)
        }
    }

    public var forks: Int? {
        didSet {
            forksLabel.text = "⑂ " + String(forks ?? 0)
        }
    }

    private let nameLabel = UILabel(frame: .zero)
    private let descriptionLabel = UILabel(frame: .zero)

    private let statsView = UIView(frame: .zero)
    private let starsLabel = UILabel(frame: .zero)
    private let forksLabel = UILabel(frame: .zero)

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        nameLabel.textColor = .black

        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.font = UIFont.systemFont(ofSize: 13)
        descriptionLabel.textColor = .deepGray
        descriptionLabel.numberOfLines = 0

        starsLabel.translatesAutoresizingMaskIntoConstraints = false
        starsLabel.font = UIFont.systemFont(ofSize: 13)
        starsLabel.textColor = .deepGray

        forksLabel.translatesAutoresizingMaskIntoConstraints = false
        forksLabel.font = UIFont.systemFont(ofSize: 13)
        forksLabel.textColor = .deepGray

        statsView.translatesAutoresizingMaskIntoConstraints = false
        statsView.addSubview(starsLabel)
        statsView.addSubview(forksLabel)

        contentView.addSubview(nameLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(statsView)

        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented; use the designated initializer instead")
    }

    func setupConstraints() {
        let constraints: [NSLayoutConstraint] = [
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16.0),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            nameLabel.trailingAnchor.constraint(equalTo: statsView.leadingAnchor, constant: -8.0),

            descriptionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8.0),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16.0),

            statsView.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            statsView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            starsLabel.topAnchor.constraint(equalTo: statsView.topAnchor),
            starsLabel.leadingAnchor.constraint(equalTo: statsView.leadingAnchor),
            starsLabel.bottomAnchor.constraint(equalTo: statsView.bottomAnchor),

            forksLabel.topAnchor.constraint(equalTo: statsView.topAnchor),
            forksLabel.leadingAnchor.constraint(equalTo: starsLabel.trailingAnchor, constant: 8.0),
            forksLabel.trailingAnchor.constraint(equalTo: statsView.trailingAnchor),
            forksLabel.bottomAnchor.constraint(equalTo: statsView.bottomAnchor),
        ]

        NSLayoutConstraint.activate(constraints)
    }
}
