//
//  ErrorDialogView.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation
import UIKit

class ErrorDialogView: UIView {
    private let overlayView: UIView

    private let promptView: UIView
    private let headerLabel: UILabel
    private let messageLabel: UILabel
    private let separatorLine: UIView
    public let okButton: UIButton

    init(header: String, message: String, confirmAction: String? = "OK") {
        overlayView = UIView(frame: .zero)

        promptView = UIView(frame: .zero)
        headerLabel = UILabel(frame: .zero)
        messageLabel = UILabel(frame: .zero)
        separatorLine = UIView(frame: .zero)
        okButton = UIButton(frame: .zero)

        super.init(frame: .zero)

        overlayView.translatesAutoresizingMaskIntoConstraints = false
        overlayView.backgroundColor = .black20

        promptView.translatesAutoresizingMaskIntoConstraints = false
        promptView.backgroundColor = .white
        promptView.layer.cornerRadius = 16.0

        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        headerLabel.text = header
        headerLabel.textColor = .black
        headerLabel.textAlignment = .center
        headerLabel.font = UIFont.systemFont(ofSize: 24.0, weight: .bold)

        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.text = message
        messageLabel.textColor = .deepGray
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
        messageLabel.font = UIFont.systemFont(ofSize: 16.0)

        separatorLine.translatesAutoresizingMaskIntoConstraints = false
        separatorLine.backgroundColor = .black20

        okButton.translatesAutoresizingMaskIntoConstraints = false
        okButton.setTitle(confirmAction, for: .normal)
        okButton.setTitleColor(.royalBlue, for: .normal)
        okButton.titleLabel?.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)

        addSubview(overlayView)

        promptView.addSubview(headerLabel)
        promptView.addSubview(messageLabel)
        promptView.addSubview(separatorLine)
        promptView.addSubview(okButton)
        addSubview(promptView)

        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not implemented; use designated initializer instead")
    }
    override init(frame: CGRect) {
        fatalError("init(frame:) not implemented; use designated initializer instead")
    }

    func setupConstraints() {
        let constraints: [NSLayoutConstraint] = [
            overlayView.topAnchor.constraint(equalTo: topAnchor),
            overlayView.leadingAnchor.constraint(equalTo: leadingAnchor),
            overlayView.trailingAnchor.constraint(equalTo: trailingAnchor),
            overlayView.bottomAnchor.constraint(equalTo: bottomAnchor),

            promptView.centerXAnchor.constraint(equalTo: centerXAnchor),
            promptView.centerYAnchor.constraint(equalTo: centerYAnchor),
            promptView.widthAnchor.constraint(greaterThanOrEqualToConstant: 280.0),
            promptView.widthAnchor.constraint(lessThanOrEqualToConstant: 350.0),
            promptView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 16.0),
            promptView.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -16.0),

            headerLabel.topAnchor.constraint(equalTo: promptView.topAnchor, constant: 24.0),
            headerLabel.leadingAnchor.constraint(equalTo: promptView.leadingAnchor, constant: 24.0),
            headerLabel.trailingAnchor.constraint(equalTo: promptView.trailingAnchor, constant: -24.0),

            messageLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 24.0),
            messageLabel.leadingAnchor.constraint(equalTo: promptView.leadingAnchor, constant: 24.0),
            messageLabel.trailingAnchor.constraint(equalTo: promptView.trailingAnchor, constant: -24.0),

            separatorLine.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 24.0),
            separatorLine.leadingAnchor.constraint(equalTo: promptView.leadingAnchor),
            separatorLine.trailingAnchor.constraint(equalTo: promptView.trailingAnchor),
            separatorLine.heightAnchor.constraint(equalToConstant: 0.5),

            okButton.widthAnchor.constraint(equalTo: promptView.widthAnchor),
            okButton.heightAnchor.constraint(equalToConstant: 64.0),
            okButton.topAnchor.constraint(equalTo: separatorLine.bottomAnchor),
            okButton.leadingAnchor.constraint(equalTo: promptView.leadingAnchor),
            okButton.trailingAnchor.constraint(equalTo: promptView.trailingAnchor),
            okButton.bottomAnchor.constraint(equalTo: promptView.bottomAnchor),
        ]

        NSLayoutConstraint.activate(constraints)
    }
}
