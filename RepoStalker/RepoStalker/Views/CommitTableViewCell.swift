//
//  CommitTableViewCell.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation
import UIKit

class CommitTableViewCell: UITableViewCell {
    public var sha: String? {
        didSet {
            shaLabel.text = sha
        }
    }

    public var authorName: String? {
        didSet {
            authorNameLabel.text = authorName
        }
    }

    public var message: String? {
        didSet {
            messageLabel.text = message
        }
    }

    private let shaLabel = UILabel(frame: .zero)
    private var shaLabelWidthConstraint: NSLayoutConstraint!
    private let authorNameLabel = UILabel(frame: .zero)
    private let messageLabel = UILabel(frame: .zero)

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        shaLabel.translatesAutoresizingMaskIntoConstraints = false
        shaLabel.font = UIFont(name: "Courier", size: 16.0)
        shaLabel.textColor = .deepGray

        authorNameLabel.translatesAutoresizingMaskIntoConstraints = false
        authorNameLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        authorNameLabel.textColor = .black
        authorNameLabel.lineBreakMode = .byTruncatingTail

        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.font = UIFont.systemFont(ofSize: 16)
        messageLabel.textColor = .deepGray
        messageLabel.numberOfLines = 0
        messageLabel.lineBreakMode = .byWordWrapping

        contentView.addSubview(shaLabel)
        contentView.addSubview(authorNameLabel)
        contentView.addSubview(messageLabel)

        setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented; use the designated initializer instead")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        shaLabelWidthConstraint.constant = shaLabel.intrinsicContentSize.width
    }

    func setupConstraints() {
        shaLabelWidthConstraint = shaLabel.widthAnchor.constraint(equalToConstant: 0)

        let constraints: [NSLayoutConstraint] = [
            shaLabelWidthConstraint,
            shaLabel.centerYAnchor.constraint(equalTo: authorNameLabel.centerYAnchor),
            shaLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),

            authorNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16.0),
            authorNameLabel.leadingAnchor.constraint(equalTo: shaLabel.trailingAnchor, constant: 16.0),
            authorNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            messageLabel.topAnchor.constraint(equalTo: authorNameLabel.bottomAnchor, constant: 8.0),
            messageLabel.leadingAnchor.constraint(equalTo: authorNameLabel.leadingAnchor),
            messageLabel.trailingAnchor.constraint(equalTo: authorNameLabel.trailingAnchor),
            messageLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16.0),
        ]

        NSLayoutConstraint.activate(constraints)
    }
}
