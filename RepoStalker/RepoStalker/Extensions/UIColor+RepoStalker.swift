//
//  UIColor+RepoStalker.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class var limeGreen: UIColor {
        return UIColor(hue: 0.3472, saturation: 1.0, brightness: 0.95, alpha: 1.0)
    }

    class var royalBlue: UIColor {
        return UIColor(hue: 0.625, saturation: 1.0, brightness: 1.0, alpha: 1.0)
    }

    class var deepGray: UIColor {
        return UIColor(hue: 0.0, saturation: 0.0, brightness: 0.2, alpha: 1.0)
    }

    class var black20: UIColor {
        return UIColor(hue: 0.0, saturation: 0.0, brightness: 0.0, alpha: 0.2)
    }

    class var white20: UIColor {
        return UIColor(hue: 0.0, saturation: 0.0, brightness: 1.0, alpha: 0.2)
    }

    class var white60: UIColor {
        return UIColor(hue: 0.0, saturation: 0.0, brightness: 1.0, alpha: 0.6)
    }
}
