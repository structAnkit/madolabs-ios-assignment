//
//  NSAttributedString+RepoStalker.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
    func attributedString(font: UIFont, spacing: Double) -> NSAttributedString {
        guard let newString = mutableCopy() as? NSMutableAttributedString else { return self }

        let range = NSRange(location: 0, length: newString.length)
        newString.addAttribute(.font, value: font, range: range)
        newString.addAttribute(.kern, value: spacing, range: range)
        return newString
    }
}
