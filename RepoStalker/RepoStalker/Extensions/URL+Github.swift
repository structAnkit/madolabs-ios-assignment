//
//  URL+Github.swift
//  RepoStalker
//
//  Created by Ankit Aggarwal on 2/20/18.
//  Copyright © 2018 Ankit Aggarwal. All rights reserved.
//

import Foundation

extension URL {
    static func githubUser(with username: String) -> URL? {
        guard !username.isEmpty else { return nil }

        return URL(string: "https://api.github.com/users/" + username)
    }

    static func githubRepos(for username: String) -> URL? {
        guard !username.isEmpty else { return nil }
        return URL(string: "https://api.github.com/users/" + username + "/repos")
    }

    static func githubCommits(for username: String, repo: String) -> URL? {
        guard !username.isEmpty, !repo.isEmpty else { return nil }
        return URL(string: "https://api.github.com/repos/" + username + "/" + repo + "/commits")
    }
}
